package com.dodola.animview.slice;

import com.dodola.animview.*;
import com.dodola.animview.library.MetaballDebugView;
import com.dodola.animview.library.MetaballView;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

/**
 * 显示Metaball效果
 */
public class MainAbilitySlice extends AbilitySlice {
    private MetaballView metaballView;
    private MetaballDebugView debugMetaballView;
    private Image image;
    private Slider seekBar;
    private Slider seekBar2;
    private Slider seekBar3;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        metaballView = (MetaballView) this.findComponentById(ResourceTable.Id_metaball);

        debugMetaballView = (MetaballDebugView) findComponentById(ResourceTable.Id_debug_metaball);
        image = (Image) findComponentById(ResourceTable.Id_image);
        seekBar = (Slider) findComponentById(ResourceTable.Id_seekBar);
        seekBar2 = (Slider) findComponentById(ResourceTable.Id_seekBar2);
        seekBar3 = (Slider) findComponentById(ResourceTable.Id_seekBar3);

        seekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int is, boolean bs) {
                debugMetaballView.setMaxDistance(is);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
        seekBar2.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int is, boolean bs) {
                debugMetaballView.setMv(is / 100f);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
        seekBar3.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int is, boolean bs) {
                debugMetaballView.setHandleLenRate(is / 100f);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });

        image.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                CustomCommonDialog customCommonDialog = new CustomCommonDialog(getContext());
                customCommonDialog.show();
                customCommonDialog.setClickListener(new CustomCommonDialog.ClickListener() {
                    @Override
                    public void onclick(Component component) {
                        switch (component.getId()) {
                            case ResourceTable.Id_action_fill:
                                metaballView.setPaintMode(1);
                                debugMetaballView.setPaintMode(1);
                                break;
                            case ResourceTable.Id_action_strock:
                                metaballView.setPaintMode(0);
                                debugMetaballView.setPaintMode(0);
                                break;
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
