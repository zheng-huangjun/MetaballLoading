/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dodola.animview;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

/**
 * 自定dialog
 */
public class CustomCommonDialog extends CommonDialog {
    private Context context;
    private CommonDialog dialog;
    private Component dialogView;
    private Text action_fill;
    private Text action_strock;

    /**
     * 点击回调事件
     */
    public ClickListener clickListener;

    /**
     * 点击接口
     */
    public interface ClickListener {
        void onclick(Component component);
    }

    /**
     * 点击事件
     * @param clickListener 事件
     */
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public CustomCommonDialog(Context context) {
        this(context, 0);
    }

    public CustomCommonDialog(Context context, int theme) {
        this(context, theme, 0);
    }

    public CustomCommonDialog(Context context, int theme, int layoutRes) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        dialogView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_dialog, null, true);
        dialog = setContentCustomComponent(dialogView);

        dialog.setDialogListener(new BaseDialog.DialogListener() {
            @Override
            public boolean isTouchOutside() {
                dialog.hide();
                return false;
            }
        });
        dialog.setSize(500, 500);
        dialog.setTransparent(false);
        action_fill = (Text) dialogView.findComponentById(ResourceTable.Id_action_fill);
        action_strock = (Text) dialogView.findComponentById(ResourceTable.Id_action_strock);
        action_fill.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (clickListener != null) {
                    clickListener.onclick(action_fill);
                    hide();
                }
            }
        });

        action_strock.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (clickListener != null) {
                    clickListener.onclick(action_strock);
                    hide();
                }
            }
        });
    }

}
