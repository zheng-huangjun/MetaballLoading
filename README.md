# MetaballLoading
A 2d metaball loading

##### 效果图
<img src="https://gitee.com/openharmony-tpc/MetaballLoading/raw/master/MetaballLoading.gif" style="zoom: 45%" />   

----------


## Install

添加库的依赖
方式一：
添加har包到lib文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects {
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:MetaballLoading:1.0.2'

##ScreenShot

##ScreenShot
用法：
    <com.dodola.animview.library.MetaballView
            ohos:id="$+id:metaball"
            ohos:height="30vp"
            ohos:width="match_parent"
            ohos:top_margin="30vp"
            ohos:layout_alignment="horizontal_center"
            ohos:below="$id:image"
    />


##Update

###1. 增加了调试模式，可以调整参数看看对图形的影响

<com.dodola.animview.library.MetaballDebugView
            ohos:id="$+id:debug_metaball"
            ohos:height="380vp"
            ohos:width="match_content"
            ohos:below="$id:metaball"
            ohos:align_right="$id:metaball"
            ohos:align_end="$id:metaball"

    />


调节最大间距
seekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int is, boolean bs) {
                debugMetaballView.setMaxDistance(is);
            }
        });

调节贝塞尔曲线角度
seekBar2.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int is, boolean bs) {
                debugMetaballView.setMv(is / 100f);
            }
        });

调节贝塞尔曲线控制点长度比率
seekBar3.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int is, boolean bs) {
                debugMetaballView.setHandleLenRate(is / 100f);
            }
        });


License
--------

The MIT License (MIT)

Copyright (c) 2015 dodola

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
