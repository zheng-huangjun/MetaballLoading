package com.dodola.animview.library;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;

/**
 * Created by dodola on 15/7/27.
 */
public class MetaballDebugView extends Component implements Component.DrawTask {
    private Paint paint = new Paint();

    private float handleLenRate = 2f;
    private final float radius = 60;
    private final float SCALE_RATE = 0.3f;
    private ArrayList<Circle> circlePaths = new ArrayList<>();
    private float mv = 0.6f;
    private float maxDistance = radius * 4;

    public MetaballDebugView(Context context) {
        super(context);
        addDrawTask(this);
        init();
    }

    public MetaballDebugView(Context context, AttrSet attrs) {
        super(context, attrs);
        addDrawTask(this);
        init();
    }

    public MetaballDebugView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addDrawTask(this);
        init();

    }

    /**
     * 获取曲线角度
     *
     * @return 角度
     */
    public float getMv() {
        return mv;
    }

    /**
     * 设置曲线角度
     *
     * @param mv 角度
     */
    public void setMv(float mv) {
        this.mv = mv;
        invalidate();
    }

    /**
     * 获取最大间距
     *
     * @return 间距
     */
    public float getMaxDistance() {
        return maxDistance;
    }

    /**
     * 设置最大间距
     *
     * @param maxDistance 间距
     */
    public void setMaxDistance(float maxDistance) {
        this.maxDistance = maxDistance;
        invalidate();
    }

    /**
     * 获取曲线长度率
     *
     * @return 长度率
     */
    public float getHandleLenRate() {
        return handleLenRate;
    }

    /**
     * 设置曲线长度率
     *
     * @param handleLenRate 长度率
     */
    public void setHandleLenRate(float handleLenRate) {
        this.handleLenRate = handleLenRate;
        invalidate();
    }


    private class Circle {
        float[] center;
        float radius;
    }

    /**
     * 设置模式
     *
     * @param mode 模式 1代表实体 0代表阴影
     */
    public void setPaintMode(int mode) {
        paint.setStyle(mode == 0 ? Paint.Style.STROKE_STYLE : Paint.Style.FILL_STYLE);
        invalidate();
    }

    private void init() {
        paint.setColor(new Color(0xff4db9ff));
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setAntiAlias(true);

        setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                Circle circle = circlePaths.get(0);
                if (touchEvent.getAction()==TouchEvent.PRIMARY_POINT_UP || touchEvent.getAction()==TouchEvent.PRIMARY_POINT_DOWN){
                    circle.center[1] = touchEvent.getPointerPosition(0).getY();
                }else {
                    circle.center[1] = touchEvent.getPointerPosition(0).getY() - 300;
                }
                circle.center[0] = touchEvent.getPointerPosition(0).getX() - 40;
                invalidate();
                return true;
            }
        });
    }

    private void initMetaballs() {
        Circle circlePath = new Circle();
        circlePath.center = new float[]{(radius), radius};
        circlePath.radius = radius;
        circlePaths.add(circlePath);

        circlePath = new Circle();
        circlePath.center = new float[]{this.getEstimatedWidth() / 2, this.getEstimatedHeight() / 3};
        circlePath.radius = radius;
        circlePaths.add(circlePath);
    }

    private float[] getVector(float radians, float length) {
        float x1 = (float) (Math.cos(radians) * length);
        float y1 = (float) (Math.sin(radians) * length);
        return new float[]{
                x1, y1
        };
    }

    /**
     * 画球
     *
     * @param canvas          画布
     * @param js              循环
     * @param is              0
     * @param vs              控制两个圆连接时候长度，间接控制连接线的粗细，该值为1的时候连接线为直线
     * @param handle_len_rate 曲线控制点长度比率
     * @param maxDistance     最大距离
     */
    private void metaball(Canvas canvas, int js, int is, float vs, float handle_len_rate, float maxDistance) {
        final Circle circle1 = circlePaths.get(is);
        final Circle circle2 = circlePaths.get(js);

        RectFloat ball1 = new RectFloat();
        ball1.left = circle1.center[0] - circle1.radius;
        ball1.top = circle1.center[1] - circle1.radius;
        ball1.right = ball1.left + circle1.radius * 2;
        ball1.bottom = ball1.top + circle1.radius * 2;

        RectFloat ball2 = new RectFloat();
        ball2.left = circle2.center[0] - circle2.radius;
        ball2.top = circle2.center[1] - circle2.radius;
        ball2.right = ball2.left + circle2.radius * 2;
        ball2.bottom = ball2.top + circle2.radius * 2;

        float[] center1 = new float[]{
                ball1.getCenter().getPointX(),
                ball1.getCenter().getPointY()
        };
        float[] center2 = new float[]{
                ball2.getCenter().getPointX(),
                ball2.getCenter().getPointY()
        };
        float ds = getDistance(center1, center2);

        float radius1 = ball1.getWidth() / 2;
        float radius2 = ball2.getWidth() / 2;
        float pi2 = (float) (Math.PI / 2);
        float u1;
        float u2;

        if (ds > maxDistance) {
            canvas.drawCircle(ball2.getCenter().getPointX(), ball2.getCenter().getPointY(), circle2.radius, paint);
        } else {
            float scale2 = 1 + SCALE_RATE * (1 - ds / maxDistance);
            radius2 *= scale2;
            canvas.drawCircle(ball2.getCenter().getPointX(), ball2.getCenter().getPointY(), radius2, paint);
        }

        if (radius1 == 0 || radius2 == 0) {
            return;
        }

        if (ds > maxDistance || ds <= Math.abs(radius1 - radius2)) {
            return;
        } else if (ds < radius1 + radius2) {
            u1 = (float) Math.acos((radius1 * radius1 + ds * ds - radius2 * radius2) /
                    (2 * radius1 * ds));
            u2 = (float) Math.acos((radius2 * radius2 + ds * ds - radius1 * radius1) /
                    (2 * radius2 * ds));
        } else {
            u1 = 0;
            u2 = 0;
        }
        float[] centermin = new float[]{center2[0] - center1[0], center2[1] - center1[1]};

        float angle1 = (float) Math.atan2(centermin[1], centermin[0]);
        float angle2 = (float) Math.acos((radius1 - radius2) / ds);
        float angle1a = angle1 + u1 + (angle2 - u1) * vs;
        float angle1b = angle1 - u1 - (angle2 - u1) * vs;
        float angle2a = (float) (angle1 + Math.PI - u2 - (Math.PI - u2 - angle2) * vs);
        float angle2b = (float) (angle1 - Math.PI + u2 + (Math.PI - u2 - angle2) * vs);

        float[] p1a1 = getVector(angle1a, radius1);
        float[] p1b1 = getVector(angle1b, radius1);
        float[] p2a1 = getVector(angle2a, radius2);
        float[] p2b1 = getVector(angle2b, radius2);

        float[] p1a = new float[]{p1a1[0] + center1[0], p1a1[1] + center1[1]};
        float[] p1b = new float[]{p1b1[0] + center1[0], p1b1[1] + center1[1]};
        float[] p2a = new float[]{p2a1[0] + center2[0], p2a1[1] + center2[1]};
        float[] p2b = new float[]{p2b1[0] + center2[0], p2b1[1] + center2[1]};

        float[] p1Andp2 = new float[]{p1a[0] - p2a[0], p1a[1] - p2a[1]};

        float totalRadius = (radius1 + radius2);
        float d2 = Math.min(vs * handle_len_rate, getLength(p1Andp2) / totalRadius);
        d2 *= Math.min(1, ds * 2 / (radius1 + radius2));
        radius1 *= d2;
        radius2 *= d2;

        float[] sp1 = getVector(angle1a - pi2, radius1);
        float[] sp2 = getVector(angle2a + pi2, radius2);
        float[] sp3 = getVector(angle2b - pi2, radius2);
        float[] sp4 = getVector(angle1b + pi2, radius1);

        Path path1 = new Path();
        path1.moveTo(p1a[0], p1a[1]);
        path1.cubicTo(new Point(p1a[0] + sp1[0], p1a[1] + sp1[1]),
                new Point(p2a[0] + sp2[0], p2a[1] + sp2[1]), new Point(p2a[0], p2a[1]));
        path1.lineTo(p2b[0], p2b[1]);
        path1.cubicTo(new Point(p2b[0] + sp3[0], p2b[1] + sp3[1]),
                new Point(p1b[0] + sp4[0], p1b[1] + sp4[1]), new Point(p1b[0], p1b[1]));
        path1.lineTo(p1a[0], p1a[1]);
        path1.close();
        canvas.drawPath(path1, paint);
    }

    private float getLength(float[] b1) {
        return (float) Math.sqrt(b1[0] * b1[0] + b1[1] * b1[1]);
    }

    private float getDistance(float[] b1, float[] b2) {
        float x1 = b1[0] - b2[0];
        float y1 = b1[1] - b2[1];
        float d1 = x1 * x1 + y1 * y1;
        return (float) Math.sqrt(d1);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (circlePaths.size() == 0) {
            initMetaballs();
        }

        final Circle circle1 = circlePaths.get(0);
        RectFloat ball1 = new RectFloat();
        ball1.left = circle1.center[0] - circle1.radius;
        ball1.top = circle1.center[1] - circle1.radius;
        ball1.right = ball1.left + circle1.radius * 2;
        ball1.bottom = ball1.top + circle1.radius * 2;
        canvas.drawCircle(ball1.getCenter().getPointX(), ball1.getCenter().getPointY(), circle1.radius, paint);

        for (int i = 1; i < circlePaths.size(); i++) {
            metaball(canvas, i, 0, mv, handleLenRate, maxDistance);
        }
    }
}
